import * as React from 'react';
import logo from './logo.svg';
import './App.css';
import CardBlog from './components/card';

const arr_=['Seanthan','Kobiga','Anbarasi','Senthur','Gowsika'];
const list = [
  {
    title: 'React',
    url: 'https://reactjs.org/',
    author: 'Jordan Walke',
    num_comments: 3,
    points: 4,
    objectID: 0,
  },
  {
    title: 'Redux',
    url: 'https://redux.js.org/',
    author: 'Dan Abramov, Andrew Clark',
    num_comments: 2,
    points: 5,
    objectID: 1,
  },
];

function getTitle(title) {
  return title;
}

function List() {
  return (
    <ul>
      {list.map(function (item) {
        return (
          <li key={item.objectID}>
            <span>
              <a href={item.url}>{item.title}</a>
            </span>
            <span>{item.author}</span>
            <span>{item.num_comments}</span>
            <span>{item.points}</span>
          </li>
        );
      })}
    </ul>
  );
}


function Card(){
  return(
    arr_.map(function (item) {
      return CardBlog(item);
    })
  );
}

function App() {
  // const title = 'Hello World';
  

  return (
    
    <div className='App'>
      <Card/>

      {/* {arr_.map(function (item) {
        return CardBlog(item);
      })} */}

      <button type='button' className='btn btn-primary'>hello</button>
      {/* <CardBlog/> */}
      {/* <div className='card'>
      <List />
      </div>
      <div className='card'>
        a<br></br>
        a<br></br>
        a<br></br>
      </div>
      <div className='card'>
      {list.map(function (item) {
          return <li>{item.title}</li>;
        })}
      </div>
      <div className='card'>
        a<br></br>
        a<br></br>
        a<br></br>
      </div> */}

      {/* <h1>{getTitle('hello react.......')}</h1>
      <label htmlFor="search">Search: </label>
      <input id="search" type="text" />

      <hr />
      <ul>
        {list.map(function (item) {
          return <li>{item.title}</li>;
        })}
      </ul>
      <hr />
      <ul>
        {list.map(function (item) {
          return <li key={item.objectID}>{item.objectID} : {item.title}</li>;
        })}
      </ul>
      <hr />
        {List()}
      <List />call List() function */}
    </div>

  );
}

export default App;
